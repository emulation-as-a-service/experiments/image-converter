import os
import sys

import requests

INSTANCE_URL = "http://localhost:8080"
ORIGINAL_IMG_ID = ""


def get_url(image_id):
    return f"{INSTANCE_URL}/emil/environment-repository/images/{image_id}/url"


def determine_and_download_file(image_id):
    print("----- ----- -----")
    print("Starting new iteration for image id:", image_id)
    url = get_url(image_id)
    print("Using url:", url)

    ans = requests.get(url, allow_redirects=False)

    print("Got headers:", ans.headers)

    loc = ans.headers["Location"]
    print("Location:", loc)
    print(f"Downloading {image_id}...")
    os.system(f'curl "{loc}" --output {image_id}')
    print(f"Determining backing file for {image_id}...")
    stream = os.popen(f'qemu-img info {image_id}')
    output = stream.readlines()

    backing_file_found = False
    for line in output:
        if line.startswith("backing file: "):
            new_id = line[14:].strip()
            print("Backing file id:", new_id)
            backing_file_found = True

    if backing_file_found:
        determine_and_download_file(new_id)
    else:
        print("No backing file specified, converting to standalone disk file...")
        os.system(f"qemu-img convert -O qcow2 {ORIGINAL_IMG_ID} fullImage.img")
        print("Finished, result: fullImage.img")


if __name__ == '__main__':
    ORIGINAL_IMG_ID = sys.argv[1]
    image_id = sys.argv[1]
    if len(sys.argv) > 2:
        INSTANCE_URL = sys.argv[2]
    determine_and_download_file(image_id)
